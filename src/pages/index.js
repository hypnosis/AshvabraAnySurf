import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import '../styles/index.css';

function Index() {
  const [date, setDate] = useState(null);
  useEffect(() => {
    async function getDate() {
      const res = await fetch('/api/date');
      const newDate = await res.text();
      setDate(newDate);
    }
    getDate();
  }, []);
  return (
    <main>
      <Helmet>
        <title>Храм Священной Ашвабры</title>
      </Helmet>
      <h1>Храм Священной Ашвабры</h1>

      <p><em><i>
        No project file specified for module <code>opalescent</code>:

        {' '}

        <a
          href="https://opalescent.now.sh/"
        >
          https://opalescent.now.sh/
        </a>{' — '} or, better, {' '}

        <a
          href="https://opalescent.any.surf/"
        >
          https://opalescent.any.surf/
        </a> {' '} (if the latter works).
      </i></em></p>

      <hr noshade="noshade" size={2} />

      <p>
        Deployed with
        {' '}
        <a
          href="https://vercel.com/docs"
          target="_blank"
          rel="noreferrer noopener"
        >
          Vercel
        </a>

        {' | '}

        Powered by
        {' '}
        <a 
          href="https://www.gatsbyjs.org/"
          target="_blank"
          rel="noreferrer noopener"
        >
          GatsbyJS
        </a>.
      </p>
    </main>
  );
}

export default Index;
