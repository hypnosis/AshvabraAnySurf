```
<Yeoman> No project file specified for module opalescent
<s> Yeoman: это на чем?
<Yeoman> s, на gatsbyjs.org
```

https://opalescent.now.sh/ https://opalescent.any.surf/

https://gatsbyjs.org/


## unmodified gatsbyjs example content follows

![Gatsby Logo](https://github.com/vercel/vercel/blob/master/packages/frameworks/logos/gatsby.svg)

# Gatsby Example

This directory is a brief example of a [Gatsby](https://www.gatsbyjs.org/) app with [Serverless Functions](https://vercel.com/docs/v2/serverless-functions/introduction) that can be deployed with Vercel and zero configuration.

## Deploy Your Own

Deploy your own Gatsby project, along with Serverless Functions, with Vercel.

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/clone?repository-url=https://github.com/vercel/vercel/tree/main/examples/gatsby&template=gatsby)

_Live Example: https://gatsby.now-examples.now.sh_

### How We Created This Example

To get started with Gatsby on Vercel, you can use the [Gatsby CLI](https://www.gatsbyjs.org/docs/gatsby-cli/) to initialize the project:

```shell
$ gatsby new gatsby-site
```
